import MySQLdb
import os
import sys
import django
import datetime
from django.utils import timezone

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ELEC3609.settings')
django.setup()

db = MySQLdb.connect(
    host="127.0.0.1",
    user="root",
    passwd="pgroup2",
    db="code_query",
    use_unicode=True
)
cur = db.cursor()
db.autocommit(on=True)

now = timezone.now()

# ----------------------------
# Empty Database
# ----------------------------
if len(sys.argv) == 2:
    print("\nSetting up empty database.\n")
    query = 'DROP DATABASE IF EXISTS code_query'
    cur.execute(query)
    query = 'CREATE DATABASE code_query'
    cur.execute(query)
    os.system(sys.argv[1] + " manage.py makemigrations Codequery")
    os.system(sys.argv[1] + " manage.py migrate")
    print("\nCreating default user account\n")
    os.system(sys.argv[1] + " manage.py createsuperuser")
else:
    print("Please run python setup-database.py [py/python/python3]")
    print("py/python/python3 depends on what system you are using.")
    exit(0)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ELEC3609.settings')
django.setup()

db = MySQLdb.connect(
    host="127.0.0.1",
    user="root",
    passwd="pgroup2",
    db="code_query",
    use_unicode=True
)

cur = db.cursor()
db.autocommit(on=True)

now = timezone.now()

