from django.test import RequestFactory
from django.core.urlresolvers import reverse
from unittest.mock import patch, MagicMock, NonCallableMock
from unittest import TestCase
from django.test import TestCase, Client
from .views import *
from django.test import tag
from .models import *
from django.contrib.auth.models import User
import django.utils.timezone as timezone
from django.contrib.auth import login, authenticate, logout
from datetime import timedelta



class TestBaseModel(TestCase):
    # Set up the mock base model for the whole class with values
    @classmethod
    def setUp(cls):
        cls.mock_base = MagicMock(BaseModel)
        cls.mock_base.created_date = cls.created_date = timezone.now()
        cls.mock_base.modified_date = cls.modified_date = timezone.now()

    # Unit test for the base model created date
    @tag('unit', 'models')
    def test_base_model_created_date(self):
        self.assertEquals(self.mock_base.created_date, self.created_date)

    # Unit test for the base model modified date
    @tag('unit', 'models')
    def test_base_model_modified_date(self):
        self.assertEquals(self.mock_base.modified_date, self.modified_date)

    # Unit test to check created and modified dates are the same
    @tag('unit', 'models')
    def test_base_model_dates_are_the_same(self):
        self.assertEquals(self.mock_base.created_date, self.mock_base.modified_date)

    # Unit test for base model to check dates are different after one has been changed
    @tag('unit', 'models')
    def test_base_model_dates_are_different_after_modification(self):
        self.mock_base.modified_date = timezone.now() + timedelta(days=1)
        self.assertNotEquals(self.mock_base.created_date, self.mock_base.modified_date)


# Unit tests for the Topic model
class TestTopicModel(TestCase):
    # Set up the mock Topic model for the whole class with values
    @classmethod
    def setUp(cls):
        cls.mock_topic = MagicMock(Topic)
        cls.mock_topic.topic_name = 'Student'

    # Unit test the Topic name
    @tag('unit', 'models')
    def test_topic_model_name(self):
        self.assertEquals(self.mock_topic.topic_name, 'Student')

    # Unit test an Topic name change still returns correct value
    @tag('unit', 'models')
    def test_topic_model_name_update(self):
        self.mock_topic.topic_name = 'Graduate'
        self.assertEquals(self.mock_topic.topic_name, 'Graduate')
        self.assertNotEquals(self.mock_topic.topic_name, 'Student')



# Unit tests for the Topic model
class TestQuestionModel(TestCase):
    # Set up the mock Topic model for the whole class with values
    @classmethod
    def setUp(cls):
        cls.mock_question = MagicMock(Question)
        cls.mock_question.question_title = 'Question title'

    # Unit test the Topic name
    @tag('unit', 'models')
    def test_question_model_title(self):
        self.assertEquals(self.mock_question.question_title, 'Question title')

    # Unit test an Topic name change still returns correct value
    @tag('unit', 'models')
    def test_question_model_title_update(self):
        self.mock_question.question_title = 'Question title 2'
        self.assertEquals(self.mock_question.question_title, 'Question title 2')
        self.assertNotEquals(self.mock_question.question_title, 'Question title')




class TestQuestionPageView(TestCase):
    def setUp(self):
        #create users
        u1 = User.objects.create(username = 'charlie',email= 'charlie@example.com',password = 'pass1234')
        u2 = User.objects.create(username = 'barry',email='barry@example.com',password = 'pass1234')
        u3 = User.objects.create(username = 'james',email='james@example.com',password = 'pass1234')

        #create topics
        t1 = Topic.topic_manager.create(
             topic_name = 'topic 1',
             topic_description = 'this is topic 1')
        t2 = Topic.topic_manager.create(
            topic_name='topic 2',
            topic_description='this is topic 2')
        t3 = Topic.topic_manager.create(
            topic_name='topic 3',
            topic_description='this is topic 3')

        #create questions
        q1 = Question.question_manager.create(
            created_date = timezone.now(),
            question_title = 'Question 1',
            question_body = 'this is question 1',
            topic = t1,
            user = u1
            )
        q2 = Question.question_manager.create(
            created_date=timezone.now(),
            question_title='Question 2',
            question_body='this is question 2',
            topic=t2,
            user=u2
        )
        q3 = Question.question_manager.create(
            created_date=timezone.now(),
            question_title='Question 3',
            question_body='this is question 3',
            topic=t3,
            user=u3
        )

        self.client = Client()



    def create_admin_user(self):
        self.username = 'test_admin'
        self.email = 'test@test.com'
        self.password = '12345678'
        self.user = User.objects.create_user(username=self.username, email=self.email, password=self.password)
        self.user.is_superuser = True
        self.user.is_staff = True
        self.user.is_active = True
        self.user.save()

        # Integration test - test admin login

    @tag('integration', 'admin')
    def test_admin_login(self):
        self.create_admin_user()
        logged_in = self.client.login(username=self.username, password=self.password)

        # Assert the super user is logged in to the current session
        self.assertTrue(logged_in)

    # Unit tests for the Base Model

# Unit tests for the Topic Page View
class TestTopicPageView(TestCase):

    # Unit test the <URL>/topic/# returns a 200 status_code when requested, no redirect
    @tag('unit', 'views')
    def test_topic_page_view_status(self):
        test_topic = Topic(topic_name='Test Topic Name', topic_description='Test Topic Description')
        test_topic.save()
        response = self.client.get('/topic/%d' % test_topic.pk)
        self.assertEqual(response.status_code, 302)

    # Unit test <URL>/topic/# uses the topic.html template
    @tag('unit', 'views')
    def test_topic_page_view_template(self):
        test_topic = Topic(topic_name='Test Topic Name', topic_description='Test Topic Description')
        test_topic.save()
        response = self.client.get('/topic/%d' % test_topic.pk, follow=True)
        self.assertTemplateUsed('topic.html')

    # Unit test <URL>/topic/# displays hardcoded title from html
    @tag('unit', 'views')
    def test_topic_page_view_display(self):
        test_topic = Topic(topic_name='Test Topic Name', topic_description='Test Topic Description')
        test_topic.save()
        response = self.client.get('/topic/%d' % test_topic.pk, follow=True)
        html = response.content.decode('utf8')
        self.assertIn('Test Topic Name', html)

