from .models import *
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from django.dispatch import receiver
import django.utils.timezone as timezone


#function to notify a user when a question which the user follows is posted
@receiver(post_save, sender = Question)
def q_notify(sender, **kwargs):
    if (kwargs['created']):
        q = kwargs['instance']

        #log this action
        ual = User_Activity_Log(
            user = q.user,
            post_type = 'question',
            post_description = q.question_title,
            post_id = q.id,
            created_date=django.utils.timezone.now()
        )

        ual.save()

        #notify followers of question.topic
        t = q.topic
        t.topic_num_questions += 1
        t.save()
        ut_list = User_Topic.user_topic_manager.filter(topic=t)
        t_note = 'A question has been posted under the topic'+t.topic_name +'[added link to question]'

        # send a notification
        for ut in ut_list:
            n = Notification(user=ut.user, notification_body=t_note)
            n.save()


        #notify users that follow this user that they posted a question
        ufu = User_Follows_User.user_follows_user_manager.filter(followed = q.user)
        u_note = q.user.username+'has posted a question'+'[add link to question]'
        for entry in ufu:
            follower = entry.follower
            n = Notification(user = follower, notification_body = u_note)
            n.save()

        #increment the number of the questions asked by a user
        u = q.user
        profile = UserProfile.user_manager.get(id = u.id)
        profile.num_questions_asked+=1
        profile.save()

@receiver(post_save, sender = Answer)
def a_notify(sender, **kwargs):
    if (kwargs['created']):
        a = kwargs['instance']

        poster = a.user
        a_profile = UserProfile.user_manager.get(user = poster)
        a_profile.num_answers_given+=1
        a_profile.save()

        q = a.question
        q.num_answers+=1
        q.save()

        #notify poster of question that an answer has been post to their question
        a_note = 'your question'+q.question_title+'[add link]'+'has recieved an answer'
        a_n = Notification(
            user = q.user,
            notification_body = a_note
        )
        a_n.save()

        # log this action
        des = 'left answer on question: '+q.question_title +'[add link to question where the answer is]'
        ual = User_Activity_Log(
            user=a.user,
            post_type='answer',
            post_description= des,
            post_id=a.id,
            created_date=django.utils.timezone.now()
        )
        ual.save()

        #notify users that follow this person that they posted an answer
        u_note = 'user: '+a.user.username+'has posted an answer to the question [add question link]'
        ufu = User_Follows_User.user_follows_user_manager.filter(followed = poster)
        for entry in ufu:
            follower = entry.follower
            u_n = Notification(user = follower, notification_body = u_note)
            u_n.save()


@receiver(post_save, sender=User)
def createUserProfile(sender, **kwargs):
    if (kwargs['created']):
        profile = UserProfile.user_manager.create(user=kwargs['instance'])


@receiver(post_save, sender = Question_Comment)
def qc_notify(sender, **kwargs):
    if (kwargs['created']):
        qc = kwargs['instance']

        #notify poster of question that a comment has been left
        q_poster = qc.question.user
        q = qc.question
        q.question_num_comments+=1
        q.save()

        note = 'you have received a comment on your question [insert link to question]'
        n = Notification(user =  q_poster, notification_body =note, created_date = timezone.now())
        n.save()

        #notify followers
        followers = User_Follows_User.user_follows_user_manager.filter(followed = qc.user)
        for entry in followers:
            follower = entry.follower
            fol_note = 'user'+qc.user.username+'has posted a comment on the question  [link to qeustion]'
            n = Notification(user = follower,notification_body = fol_note, created_date = timezone.now())
            n.save()

        des = 'question comment for question [insert question link]'
        #add to log
        ual = User_Activity_Log(
            user=qc.user,
            post_type='question_comment',
            post_description=des,
            post_id=qc.id,
            created_date=django.utils.timezone.now()
        )
        ual.save()


@receiver(post_save, sender=Answer_Comment)
def ac_notify(sender, **kwargs):
    if (kwargs['created']):
        ac = kwargs['instance']

        a = ac.answer
        a.num_comments+=1
        a.save()

        # notify poster of answer that a comment has been left
        a_poster = ac.answer.user
        note = 'you have received a comment on your answer [insert link to answer]'
        n = Notification(user=a_poster, notification_body=note, created_date=timezone.now())
        n.save()

        # notify followers
        followers = User_Follows_User.user_follows_user_manager.filter(followed=ac.user)
        for entry in followers:
            follower = entry.follower
            fol_note = 'user' + ac.user.username + 'has posted a comment on the answer  [link to answer]'
            n = Notification(user=follower, notification_body=fol_note, created_date=timezone.now())
            n.save()

        des = 'answer comment for answer [insert question link]'
        # add to log
        ual = User_Activity_Log(
            user=ac.user,
            post_type='answer_comment',
            post_description=des,
            post_id=ac.id,
            created_date=django.utils.timezone.now()
        )
        ual.save()
