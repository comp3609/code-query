from django.apps import AppConfig

class CodequeryConfig(AppConfig):
    name = 'Codequery'

    def ready(self):
        import Codequery.signals
