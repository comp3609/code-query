from django.conf.urls import *
from . import views
from haystack.views import SearchView, search_view_factory
from haystack.forms import SearchForm

from django.contrib.auth.views import password_reset, password_reset_done, password_reset_confirm, password_reset_complete

from rest_framework import routers
# from Codequery.views import MySearchView


router = routers.DefaultRouter()
router.register(r'users', views.UserProfileViewSet)
router.register(r'topics', views.TopicViewSet)
router.register(r'questions', views.QuestionViewSet)





urlpatterns = [
    url(r'^user_questions/(?P<user_id>\d+)$', views.user_questions, name='user_questions'),
    url(r'^user_answers/(?P<user_id>\d+)$', views.user_answers, name='user_answers'),
    url(r'^user_topics/(?P<user_id>\d+)$', views.user_topics, name='user_topics'),
    url(r'^user_achievements/(?P<user_id>\d+)$', views.user_achievements, name='user_achievements'),

    url(r'^$', views.index, name='index'),

    #urls for questions page
    url(r'^questions/$', views.questions, name='questions'),
    url(r'^questions/page-(?P<page_number>\d+)$', views.questions, name='questions'),
    url(r'^questions/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)$', views.questions, name='questions'),
    url(r'^questions/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)/unanswered-(?P<unanswered>true|false)$', views.questions, name='questions'),

    # urls for user_favorite_questions page
    url(r'^user_favorite_questions/(?P<user_id>\d+)$', views.user_favorite_questions, name='user_favorite_questions'),
    url(r'^user_favorite_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.user_favorite_questions, name='user_favorite_questions'),
    url(r'^user_favorite_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)$', views.user_favorite_questions, name='user_favorite_questions'),
    url(r'^user_favorite_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)/unanswered-(?P<unanswered>true|false)$',views.user_favorite_questions, name='user_favorite_questions'),

    #urls for users page
    url(r'^users$', views.users, name='users'),
    url(r'^users/page-(?P<page_number>\d+)$', views.users, name='users'),
    url(r'^users/page-(?P<page_number>\d+)/filter-(?P<filter>newest|rep_points)$', views.users, name='users'),


    url(r'^index/$', views.index, name='index'),

    url(r'^question/(?P<question_id>\d+)$', views.question_thread, name='question_thread'),
    url(r'^question/(?P<question_id>\d+)/filter-(?P<filter>newest|oldest|votes)$', views.question_thread, name='question_thread'),

    url(r'^user/(?P<user_id>\d+)$', views.user, name='user'),

    #urls for help pages
    url(r'^help/$', views.help, name='help'),
    url(r'^help/(?P<help_id>\d+)$', views.help_item, name='help_item'),

    url(r'^user_notifications/(?P<user_id>\d+)$', views.user_notifications, name='user_notifications'),
    url(r'^user_notifications/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.user_notifications, name='user_notifications'),

    url(r'^achievements/$', views.achievements, name='achievements'),


    #urls for user_questions
    url(r'^user_questions/(?P<user_id>\d+)$', views.user_questions, name='user_questions'),
    url(r'^user_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.user_questions, name='user_questions'),
    url(r'^user_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)$', views.user_questions, name='user_questions'),
    url(r'^user_questions/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)/unanswered-(?P<unanswered>true|false)$',views.user_questions, name='user_questions'),

    # urls for user_answers
    url(r'^user_answers/(?P<user_id>\d+)$', views.user_answers, name='user_answers'),
    url(r'^user_answers/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.user_answers, name='user_answers'),
    url(r'^user_answers/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)$', views.user_answers, name='user_answers'),
    url(r'^user_answers/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)/accepted-(?P<accepted>true|false)$', views.user_answers, name='user_answers'),

    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^create_question/$', views.create_question, name='create-question'),
    url(r'^create_answer/$', views.create_answer , name='create-answer'),
    url(r'^edit_profile/(?P<user_id>\d+)$', views.edit_profile, name='edit_profile'),
    url(r'^question_upvote/([0-9]+)$', views.question_upvote, name='question_upvote'),
    url(r'^question_downvote/([0-9]+)$', views.question_downvote, name='question_downvote'),
    url(r'^create_answer_2/([0-9]+)$', views.create_answer_2, name='create-answer_2'),
    url(r'^favorite_question/([0-9]+)$', views.favorite_question, name='favorite_question'),
   # url(r'^favorite_questions_page/([0-9]+)$', views.favorite_qesutions_page, name='favorite_questions_page'),
    # url(r'^answer/([0-9]+)/$',views.create_answer),
    url(r'^create_question_comment/([0-9]+)$', views.create_question_comment, name='create_question_comment'),
    url(r'^login/$', views.login_view, name='login'),
    url(r'^signup/$', views.signup, name='signup'),

    #message related urls
    url(r'^inbox/(?P<user_id>\d+)$', views.inbox, name='inbox'),
    url(r'^sent_messages/(?P<user_id>\d+)$', views.sent_messages, name='sent_messages'),
    url(r'^archived_messages/(?P<user_id>\d+)$', views.archived_messages, name='archived_messages'),
    url(r'^deleted_messages/(?P<user_id>\d+)$', views.deleted_messages, name='deleted_messages'),

    url(r'^write_message/([0-9]+)$', views.write_message, name='write_message'),
    url(r'^archive_message/([0-9]+)$', views.archive_message, name='archive_message'),
    url(r'^delete_message/([0-9]+)$', views.delete_message, name='delete_message'),

    url(r'^follow_topic/([0-9]+)$', views.follow_topic, name='follow_topic'),
    url(r'^follow_user/([0-9]+)$', views.follow_user, name='follow_user'),
    url(r'^accept_answer/([0-9]+)$', views.accept_answer, name='accept_answer'),
    #search Url
    # url(r'^search/', include('haystack.urls')),
    # url(r'^search/$',search_view_factory(view_class=SearchView, form_class=SearchForm), name='search'),
    url(r'^mysearch/$', views.search_questions, name='search_questions'),
    url(r'^topicsearch/$', views.search_topics, name='search_topics'),

    url(r'^contact/$', views.contact, name='contact'),
    url(r'^404/$', views.error, name='404'),
    url(r'^usersearch/$', views.search_users, name='search_users'),
    url(r'^reset-password/$', password_reset, {'template_name':
        'reset_password.html', 'post_reset_redirect':
        'password_reset_done'}, name='reset_password'),

    url(r'^reset-password/done/$', password_reset_done,{'template_name': 'reset_password_done.html'}, name='password_reset_done'),
    url(r'^reset-password/confirm/(?P<uidb64>[0-9A-Za-z]+)=(?P<token>.+)$', password_reset_confirm, {'template_name': 'reset_password_confirm.html',
                                                                                                     'post_reset_redirect': 'password_reset_complete'},
        name='password_reset_confirm'),
    url(r'^reset-password/complete/$', password_reset_complete, {'template_name': 'reset_password_complete.html'}, name='password_reset_complete'),



    url(r'^topics/$', views.topics, name='topics'),
    url(r'^topics/page-(?P<page_number>\d+)/(?:filter-(?P<filter>newests|votes)/)?$', views.topics, name='topics'),

    #urls for topic page
    url(r'^topic/(?P<topic_id>\d+)$', views.topic, name='topic'),
    url(r'^topic/(?P<topic_id>\d+)/page-(?P<page_number>\d+)$', views.topic, name='topic'),
    url(r'^topic/(?P<topic_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)$', views.topic, name='topic'),
    url(r'^topic/(?P<topic_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>newest|votes)/unanswered-(?P<unanswered>true|false)$', views.topic, name='topic'),


    #urls for topics page
    url(r'^topics/$', views.topics, name='topics'),
    url(r'^topics/page-(?P<page_number>\d+)$', views.topics, name='topics'),
    url(r'^topics/page-(?P<page_number>\d+)/filter-(?P<filter>name|num_questions)$', views.topics, name='topics'),


    #urls for user_topics
    url(r'^user_topics/(?P<user_id>\d+)$', views.user_topics, name='user_topics'),
    url(r'^user_topics/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.user_topics, name='user_topics'),
    url(r'^user_topics/(?P<user_id>\d+)/page-(?P<page_number>\d+)/filter-(?P<filter>name|num_questions)$', views.user_topics, name='user_topics'),

    #string_lst = ['fun', 'dum', 'sun', 'gum']
    #x="I love to have fun."topic_name'

    url(r'^vote_on_post/(?P<post_id>\d+)/(?P<post_type>q|a|ac|qc)/(?P<vote_type>up_vote|down_vote)$', views.vote_on_post, name='vote_on_post'),


    url(r'^followed_users/(?P<user_id>\d+)$', views.followed_users, name='followed_users'),
    url(r'^followed_users/(?P<user_id>\d+)/page-(?P<page_number>\d+)$', views.followed_users, name='followed_users'),
    #print re.findall(r"(?=("+'|'.join(string_lst)+r"))",x)

    #https://stackoverflow.com/questions?sort=frequent
    #url(r'/(?:page-(?P<page_number>\d+)/)?$', comments),  # good

    url(r'^api/', include(router.urls)),
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),

    url(r'^edit_question/(?P<q_id>\d+)$', views.edit_question, name='edit_question'),
    url(r'^edit_answer/(?P<a_id>\d+)$', views.edit_answer, name='edit_answer'),
    url(r'^edit_question_comment/(?P<qc_id>\d+)$', views.edit_question_comment, name='edit_question_comment'),
    url(r'^edit_answer_comment/(?P<ac_id>\d+)$', views.edit_answer_comment, name='edit_answer_comment'),


    #url(r'^testdb$', testdb.testdb),
]
