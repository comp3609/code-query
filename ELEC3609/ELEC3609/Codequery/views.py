# Forms
from Codequery.forms import forms
from django.shortcuts import render,redirect
# from .forms import *
from django.shortcuts import render
from django.http import (
    HttpResponseNotAllowed, HttpResponseRedirect,
    HttpResponseForbidden, HttpResponseNotFound,
)
from .models import *
from django.db.models import Q
import random
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm, UserChangeForm
from .forms import *
from django.urls import reverse
from haystack.query import SearchQuerySet
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework import status
from .serializers import UserProfileSerializer,QuestionSerializer, TopicSerializaer
from .models import UserProfile, Question, Topic


def user_notifications(request,user_id, page_number = '1'):

    u = User.objects.get(id=user_id)
    my_account = request.user == u
    if request.user != u:
        return HttpResponseForbidden('cannot perform action')

        # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)

    # data to send to the template
    context = {
        'notifications': [],
        'next_page': next_page,
        'prev_page': prev_page,
        'my_account':my_account
    }

    # Query the database
    # each users page should display 30 users, go to the next users page to display the next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    notifications = Notification.notification_manager.filter(user=u).order_by('-created_date')[lb:ub]

    for n in notifications:
        context['notifications'].append({
            'id':n.id,
            'body': n.notification_body,
        })

    return render(request,'user_notifications.html',context)




def followed_users(request, user_id ,page_number = '1'):

    #calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int+1)
    prev_page = str(page_number_int-1)

    u = User.objects.get(id = user_id)
    my_account = request.user == u

    # data to send to the template
    context = {
        'users':[],
        'next_page': next_page,
        'prev_page': prev_page,
        'my_account':my_account
    }

    # Query the database
    #each users page should display 30 users, go to the next users page to display the next 30 users
    #what order should we choose?
    lb = 30*(page_number_int-1)
    ub = 30*page_number_int

    users = User_Follows_User.user_follows_user_manager.filter(follower=u)[lb:ub]
    #users = UserProfile.user_manager.order_by(order)[lb:ub]

    for a in users:
        user = UserProfile.user_manager.get(user = a.followed)
        user_data = {
            'id': user.id,
            'user_name': user.user.username,
            'description': user.user_description,
            'picture': user.profile_picture,
            'num_followers': user.num_followers,
            'num_questions': user.num_questions_asked,
            'num_answers': user.num_answers_given,
            'member_since': user.created_date,
            'views': user.profile_views,
            'reputation': user.rep_points,
        }

        context['users'].append(user_data)

    return render(request, 'followed_users.html', context)

def user_favorite_questions(request, user_id, page_number='1', filter='newest', unanswered='false'):

    # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)

    # Query the database
    # each users page should display 30 users, go to next users page to display next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    if filter == 'newest':
        order = 'question__created_date'
    else:
        order = 'question__up_votes'

    u = User.objects.get(id=user_id)
    profile = UserProfile.user_manager.get(user=u)

    my_account = request.user == u

    # data to send to the template
    context = {
        'questions': [],
        'next_page': next_page,
        'prev_page': prev_page,
        'my_account': my_account
    }

    if unanswered == 'true':
        fav_questions = User_Favorite_Questions.user_favorite_qeustions_manager.filter(user=u).filter( question__has_accepted_answer=False).order_by(order)[lb:ub]
    else:
        fav_questions = User_Favorite_Questions.user_favorite_qeustions_manager.filter(user=u).order_by(order)[lb:ub]

    for fq in fav_questions:
        question = fq.question
        question_data = {
            'id': question.id,
            'title': question.question_title,
            'body': question.question_body,
            'date_posted': question.created_date,
            'date_last_modified': question.modified_date,
            'upVotes': question.up_votes,
            'downVotes': question.down_votes,
            'num_answers': question.num_answers,
            'num_views': question.question_num_views,
            'topic_name': question.topic.topic_name,
            'user_name': question.user.username,
            'user_picture': profile.profile_picture,
            'reputation': profile.rep_points,
        }

        context['questions'].append(question_data)

    return render(request, 'user_favorite_questions.html', context)

def user_questions(request,user_id, page_number ='1',filter='newest', unanswered='false'):

        # calculate index for next and previous page
        page_number_int = int(page_number)
        next_page = str(page_number_int + 1)
        prev_page = str(page_number_int - 1)



        # Query the database
        # each users page should display 30 users, go to next users page to display next 30 users
        # what order should we choose?
        lb = 30 * (page_number_int - 1)
        ub = 30 * page_number_int

        if filter == 'newest':
            order = 'created_date'
        else:
            order = 'up_votes'

        u = User.objects.get(id = user_id)
        profile = UserProfile.user_manager.get(user = u)

        my_account = request.user == u

        # data to send to the template
        context = {
            'questions': [],
            'next_page': next_page,
            'prev_page': prev_page,
            'my_account': my_account,
            'user': u,
        }

        if unanswered == 'true':
            questions = Question.question_manager.filter(user = u).filter(has_accepted_answer = False).order_by(order)[lb:ub]
        else:
            questions = Question.question_manager.filter(user = u).order_by(order)[lb:ub]

        for question in questions:
            question_data = {
                'id': question.id,
                'title': question.question_title,
                'body': question.question_body,
                'date_posted': question.created_date,
                'date_last_modified': question.modified_date,
                'upVotes': question.up_votes,
                'downVotes': question.down_votes,
                'num_answers': question.num_answers,
                'num_views': question.question_num_views,
                'topic_name': question.topic.topic_name,
                'user_name': question.user.username,
                'user_picture': profile.profile_picture,
                'reputation': profile.rep_points,
            }

            context['questions'].append(question_data)

        return render(request,'user_questions.html',context)


def user_topics(request,user_id, page_number='1', filter='topic_num_questions'):

        # calculate index for next and previous page
        page_number_int = int(page_number)
        next_page = str(page_number_int + 1)
        prev_page = str(page_number_int - 1)



        # Query the database
        # each users page should display 30 users, go to next users page to display next 30 users
        # what order should we choose?
        lb = 30 * (page_number_int - 1)
        ub = 30 * page_number_int

        # Query the database
        u = User.objects.get(id=user_id)

        my_account = request.user == u

        # data to send to the template
        context = {
            'topics': [],
            'next_page': next_page,
            'prev_page': prev_page,
            'my_account':my_account
        }
        #get all topics the user follows and order them

        if filter == 'name':
            ut = User_Topic.user_topic_manager.filter(user=u).order_by('topic__topic_name')[lb:ub]
        else:
            ut = User_Topic.user_topic_manager.filter(user=u).order_by('topic__topic_num_questions')[lb:ub]

        for entry in ut:
            topic = entry.topic
            topic_data = {
                'id': topic.id,
                'name': topic.topic_name,
                'description': topic.topic_description,
                'num_questions': topic.topic_num_questions,
            }

            context['topics'].append(topic_data)

        return render(request,'user_topics.html',context)

def user_answers(request,user_id, page_number='1', filter='topic_num_questions', accepted = 'false'):

    # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)



    # Query the database
    # each users page should display 30 users, go to next users page to display next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    if filter == 'newest':
        order = 'created_date'
    else:
        order = 'up_votes'

    print(user_id)
    u = User.objects.get(id=user_id)
    my_account = request.user == u

    # data to send to the template
    context = {
        'answers': [],
        'next_page': next_page,
        'prev_page': prev_page,
        'my_account': my_account,
        'user': u,
    }

    if accepted == 'true':
        answers = Answer.answer_manager.filter(user=u).filter(accepted=True).order_by(order)[lb:ub]
    else:
        answers = Answer.answer_manager.filter(user=u).order_by(order)[lb:ub]

    for a in answers:
        answer_data = {
            'q_id': a.question.id,
            'q_title': a.question.question_title,
            'a_id':a.id,
            'up_votes': a.up_votes,
            'down_downvotes': a.down_votes,
            'body': a.answer_body,
            'accepted':a.accepted,
        }

        context['answers'].append(answer_data)

    return render(request,'user_answers.html',context)

def user_achievements(request,user_id):

    # Query the database
    u = User.objects.get(id = user_id)
    my_account = request.user == u
    achievements = User_Achievement.user_achievement_manager.filter(user = u)

    # data to send to the template
    context = {
        'achievements': [],
        'my_account':my_account,
        'user': u
    }

    for a in achievements:
        achievements_data = {
            'id': a.id,
            'name': a.achievement_name,
            'description': a.achievement_description,
            'icon':a.achievement_icon,
        }

        context['achievements'].append(achievements_data)


    return render(request,'user_achievements.html',context)

def accept_answer(request, a_id):

    answer = Answer.answer_manager.get(id = a_id)
    q = answer.question
    q_u = q.user

    if request.user != q_u:
       return HttpResponseForbidden('you do not have permission to access this resource')

    u = UserProfile.user_manager.get(user=answer.user)

    accepted_answer = q.answer_set.all().filter(accepted = True)
    if accepted_answer:
        accepted_answer = accepted_answer[0]
        #can we compare a query set to single item in this way?
        if answer != accepted_answer:
            answer.accepted = True
            if (q.up_votes-q.down_votes) <= 0:
                points = 5
                u.rep_points += points
            else:
                points = 5*(q.up_votes-q.down_votes)
                u.rep_points += points

            accepted_answer.accepted = False
            accepted_answer.save()

            note = '+'+str(points)+'reputation points gain [link to source of gain]'
            n = Notification(user=answer.user, notification_body=note, created_date=timezone.now())
            n.save()

        else:
            answer.accepted = False
            q.has_accepted_answer =False

    else:
        answer.accepted = True
        q.has_accepted_answer = True

        if (q.up_votes - q.down_votes) <= 0:
            points = 5
            u.rep_points += points
        else:
            points = 5 * (q.up_votes - q.down_votes)
            u.rep_points += points

        note = '+' + str(points) + 'reputation points gain [link to source of gain]'
        n = Notification(user=answer.user, notification_body=note, created_date=timezone.now())
        n.save()

    u.save()
    q.save()
    answer.save()

    url = '/question/'+str(q.id)
    return redirect(url)

def follow_user(request, u_id):

    #note at the moment a user can follow thereself, change this
    u1 = request.user
    u2 = User.objects.get(id = u_id)

    if u1 == u2:
        return HttpResponseForbidden('could not perform action')

    ufu = User_Follows_User.user_follows_user_manager.filter(follower = u1, followed = u2)
    if ufu:
        ufu[0].delete()
    else:
        new_ufu = User_Follows_User(follower = u1, followed = u2)
        new_ufu.save()



    return redirect('/user/'+u_id)


def follow_topic(request, t_id):
    t = Topic.topic_manager.get(id = t_id)
    u = request.user
    ut = User_Topic.user_topic_manager.filter(user= u, topic =t)

    if ut:
        ut[0].delete()
    else:
        new_ut = User_Topic(user = u, topic = t)
        new_ut.save()

    return redirect('/topic/'+t_id)


def delete_message(request, m_id):

    m = Message.message_manager.get(id = m_id)
    if m.recipient != request.user:
        return HttpResponseForbidden('cannot perfrom this aciton')

    m.deleted = True
    m.save()
    return redirect(reverse('inbox'))

def archive_message(request, m_id):

    m = Message.message_manager.get(id = m_id)
    if m.recipient != request.user:
        return HttpResponseForbidden('cannot perfrom this aciton')

    m.archived = True
    m.save()
    return redirect(reverse('inbox'))


def deleted_messages(request,user_id):

    u = User.objects.get(id=user_id)
    my_account = request.user == u

    if not my_account:
        return HttpResponseForbidden('cannot perform action')

        # data to send to the template
    context = {
        'messages': [],
        'my_account': my_account
    }

    # Query the database
    #messages = Deleted_Messages.deleted_message_manager.filter(user=request.user)
    messages = Message.message_manager.filter(recipient = request.user, archived = False, deleted = True).order_by('created_date')

    for m in messages:

        sender = UserProfile.user_manager.get(user = m.sender)
        recipient = UserProfile.user_manager.get(user = m.recipient)

        message_data = {
                'id': m.id,
                'body': m.message_body,
                'time_sent': m.created_date,

        #info for sender
                'sender_id': sender.user.id,
                'sender_name':sender.user.username,
                'sender_rep': sender.rep_points,
                'sender_description': sender.user_description,
                'sender_picture': sender.profile_picture,

        #info for the recipient
                'recipient_id': recipient.user.id,
                'recipient_name':recipient.user.username,
                'recipient_rep': recipient.rep_points,
                'recipient_description': recipient.user_description,
                'recipient_picture': recipient.profile_picture,

        }

        context['messages'].append(message_data)

    return render(request, 'deleted_messages.html', context)


def archived_messages(request,user_id):

    u = User.objects.get(id=user_id)
    my_account = request.user == u

    if not my_account:
        return HttpResponseForbidden('cannot perform action')

        # data to send to the template
    context = {
        'messages': [],
        'my_account': my_account
    }

    # Query the database
    messages = Message.message_manager.filter(recipient = request.user, archived = True, deleted = False).order_by('created_date')

    for m in messages:

        sender = UserProfile.user_manager.get(user = m.sender)
        recipient = UserProfile.user_manager.get(user = m.recipient)

        message_data = {
                'id': m.id,
                'body': m.message_body,
                'time_sent': m.created_date,

        #info for sender
                'sender_id': sender.user.id,
                'sender_name':sender.user.username,
                'sender_rep': sender.rep_points,
                'sender_description': sender.user_description,
                'sender_picture': sender.profile_picture,

        #info for the recipient
                'recipient_id': recipient.user.id,
                'recipient_name':recipient.user.username,
                'recipient_rep': recipient.rep_points,
                'recipient_description': recipient.user_description,
                'recipient_picture': recipient.profile_picture,

        }

        context['messages'].append(message_data)

    return render(request, 'archived_messages.html', context)

def sent_messages(request, user_id):

    u = User.objects.get(id=user_id)
    my_account = request.user == u

    if not my_account:
        return HttpResponseForbidden('cannot perform action')

        # data to send to the template
    context = {
        'messages': [],
        'my_account': my_account
    }

    # Query the database
    messages = Message.message_manager.filter(sender = request.user, deleted = False, archived = False).order_by('-created_date')

    for m in messages:

        sender = UserProfile.user_manager.get(user = m.sender)
        recipient = UserProfile.user_manager.get(user = m.recipient)

        message_data = {
                'id': m.id,
                'body': m.message_body,
                'time_sent': m.created_date,

        #info for sender
                'sender_id': sender.user.id,
                'sender_name':sender.user.username,
                'sender_rep': sender.rep_points,
                'sender_description': sender.user_description,
                'sender_picture': sender.profile_picture,

        #info for the recipient
                'recipient_id': recipient.user.id,
                'recipient_name':recipient.user.username,
                'recipient_rep': recipient.rep_points,
                'recipient_description': recipient.user_description,
                'recipient_picture': recipient.profile_picture,

        }

        context['messages'].append(message_data)

    return render(request, 'sent_messages.html', context)

def inbox(request, user_id):

    u = User.objects.get(id = user_id)
    my_account = request.user == u

    if not my_account:
       return HttpResponseForbidden('cannot perform action')

    #data to send to the template
    context = {
        'messages':[],
        'my_account':my_account
    }

    # Query the database
    messages = Message.message_manager.filter(recipient = request.user, deleted = False, archived = False).order_by('-created_date')

    for m in messages:

        sender = UserProfile.user_manager.get(user = m.sender)
        recipient = UserProfile.user_manager.get(user = m.recipient)

        message_data = {
                'id': m.id,
                'body': m.message_body,
                'time_sent': m.created_date,

        #info for sender
                'sender_id': sender.user.id,
                'sender_name':sender.user.username,
                'sender_rep': sender.rep_points,
                'sender_description': sender.user_description,
                'sender_picture': sender.profile_picture,

        #info for the recipient
                'recipient_id': recipient.user.id,
                'recipient_name':recipient.user.username,
                'recipient_rep': recipient.rep_points,
                'recipient_description': recipient.user_description,
                'recipient_picture': recipient.profile_picture,

        }

        context['messages'].append(message_data)

    return render(request, 'inbox.html', context)



def write_message(request,u_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        form = WriteMessageForm
        user_id = u_id
        return render(request,'write_message.html',{'form': form, 'user_id':user_id,})
    elif request.method == 'POST':
        form = WriteMessageForm(request.POST)
        if form.is_valid():
            u = UserProfile.user_manager.get(id = u_id).user
            m = Message(
                message_body = form.cleaned_data.get('message_body'),
                sender = request.user,
                recipient = u
            )
            m.save()
            #return HttpResponseRedirect('/question/{0}/'.format(p.id))
            url = '/user/'+str(u_id)
            return HttpResponseRedirect(url)
        else:
            return render(request,'write_message.html', {'form': form, })
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))


def create_question_comment(request, q_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'POST':
        form = QustionCommentForm(request.POST)
        if form.is_valid():
            q = Question.question_manager.get(id =q_id)
            cq = Question_Comment(
                question_comment_body = form.cleaned_data.get('question_comment_body'),
                user = request.user,
                question = q
            )
            cq.save()
            url = '/question/'+str(q_id)
            return redirect(url)
        else:
            return redirect('/questions/')
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def favorite_question(request,q_id):

    q = Question.question_manager.get(id = q_id)
    ufq = User_Favorite_Questions.user_favorite_qeustions_manager.filter(user = request.user, question = q)
    poster = UserProfile.user_manager.get(user=q.user)

    if request.user == q.user:
        return HttpResponseForbidden('You cannot favorite your own question!')

    if ufq:
        ufq[0].delete()
        poster.rep_points-=5
    else:
        u = User_Favorite_Questions(user = request.user,question = q)
        u.save()
        poster.rep_points += 5

    poster.save()
    url = '/question/'+ q_id
    return redirect(url)

def create_question(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        form = QuestionForm()
        return render(request,'create_question.html',{'form': form,})
    elif request.method == 'POST':
        form = QuestionForm(request.POST)
        if form.is_valid():
            p = Question(
                question_title = form.cleaned_data.get('question_title'),
                question_body =form.cleaned_data.get('question_body'),
                user =request.user,
                topic = form.cleaned_data.get('topic')
            )
            p.save()
            #return HttpResponseRedirect('/question/{0}/'.format(p.id))
            return HttpResponseRedirect('/questions/')
        else:
            return render(request,'create_question.html', {'form': form, })
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def create_answer_2(request, q_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            a = Answer(
                answer_body = form.cleaned_data.get('answer_body'),
                user = request.user,
                question = Question.question_manager.get(id = q_id)
            )
            a.save()
            return HttpResponseRedirect('/questions/')
        else:
            return render(request,'questions.html')
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def create_answer(request):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        form =AnswerForm()
        return render(request,'create_answer.html',{'form': form,})
    elif request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            p = Answer(
                answer_body =form.cleaned_data.get('answer_body'),
                user =request.user,
                question = form.cleaned_data.get('question')
            )
            p.save()
            #return HttpResponseRedirect('/question/{0}/'.format(p.id))
            return HttpResponseRedirect('/questions/')
        else:
            return render(request,'create_answer.html', {'form': form, })
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def edit_question(request, q_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        q = Question.question_manager.get(id = q_id)
        form =QuestionForm(instance= q)

        my_account = request.user == q.user
        if not my_account:
            return HttpResponseForbidden('cannot perform action')
        context = {
            'form': form,
            'my_account': my_account,
            'id': q_id
        }
        return render(request, 'edit_question.html', context)
    elif request.method == 'POST':
        q = Question.question_manager.get(id = q_id)
        form = QuestionForm(request.POST, instance = q)
        if form.is_valid():
            form.save()
            return redirect('/question/'+q_id)
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def edit_answer(request, a_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        a = Answer.answer_manager.get(id = a_id)
        form =AnswerForm(instance= a)

        my_account = request.user == a.user
        if not my_account:
            return HttpResponseForbidden('cannot perform action')
        context = {
            'form': form,
            'my_account': my_account,
            'id': a_id
        }
        return render(request, 'edit_answer.html',context)
    elif request.method == 'POST':
        a = Answer.answer_manager.get(id = a_id)
        form = AnswerForm(request.POST, instance = a)
        if form.is_valid():
            form.save()
            return redirect('/question/'+str(a.question.id))
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def edit_question_comment(request, qc_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':
        qc = Question_Comment.question_comment_manager.get(id = qc_id)
        form =QustionCommentForm(instance= qc)

        my_account = request.user == qc.user
        if not my_account:
            return HttpResponseForbidden('cannot perform action')
        context = {
            'form': form,
            'my_account': my_account,
            'id': qc_id
        }

        return render(request, 'edit_question_comment.html', context)
    elif request.method == 'POST':
        qc = Question_Comment.question_comment_manager.get(id=qc_id)
        form = QustionCommentForm(request.POST, instance = qc)
        if form.is_valid():
            form.save()
            return redirect('/question/'+str(qc.question.id))
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def edit_answer_comment(request, ac_id):
    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')
    elif request.method == 'GET':

        ac = Answer_Comment.answer_comment_manager.get(id = ac_id)
        form =AnswerCommentForm(instance= ac)

        my_account = request.user == ac.user
        if not my_account:
            return HttpResponseForbidden('cannot perform action')
        context = {
            'form': form,
            'my_account': my_account,
            'id': ac_id
        }
        return render(request, 'edit_answer_comment.html', context)

    elif request.method == 'POST':
        ac = Answer_Comment.answer_comment_manager.get(id=ac_id)
        form = AnswerCommentForm(request.POST, instance = ac)
        if form.is_valid():
            form.save()
            return redirect('/question/'+str(ac.answer.question.id))
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def edit_profile(request, user_id):

    if not request.user.is_authenticated:
        return HttpResponseRedirect('/login/')

    elif request.method == 'GET':
        u = User.objects.get(id=user_id)
        my_account = request.user == u
        if not my_account:
            return HttpResponseForbidden('cannot perform action')
        form =EditProfileForm(instance=u)
        context = {
            'form': form,
            'my_account':my_account,
            'id':user_id
        }
        return render(request, 'edit_profile.html', context)
    elif request.method == 'POST':
        u = User.objects.get(id=user_id)
        form = EditProfileForm(request.POST, instance = u)
        if form.is_valid():
            form.save()
            return redirect('/user/'+user_id)
    else:
        return HttpResponseNotAllowed('{0} Not allowed'.format(request.method))

def vote_on_post(request,post_id,post_type, vote_type):

    #find out the type of post being voted on
    if post_type == 'q':
        post = Question.question_manager.get(id = post_id)
        upv = User_Question_Vote.user_question_vote_manager.filter(user = request.user, question = post)
        upv_new = User_Question_Vote(user = request.user, question = post, vote = vote_type)
        question_id = post.id
    elif post_type == 'qc':
        post = Question_Comment.question_comment_manager.get(id = post_id)
        upv = User_Question_Comment_Vote.user_question_comment_vote_manager.filter(user = request.user, qc = post)
        upv_new = User_Question_Comment_Vote(user=request.user, qc=post, vote=vote_type)
        question_id = post.question.id
    elif post_type == 'ac':
        post = Answer_Comment.answer_comment_manager.get(id = post_id)
        upv = User_Answer_Comment_Vote.user_answer_comment_vote_manager.filter(user = request.user, ac = post)
        upv_new = User_Answer_Comment_Vote(user=request.user, ac=post, vote=vote_type)
        question_id = post.answer.question.id
    else:
        post = Answer.answer_manager.get(id = post_id)
        upv = User_Answer_Vote.user_answer_vote_manager.filter(user = request.user, answer = post)
        upv_new = User_Answer_Vote(user=request.user, answer=post, vote=vote_type)
        question_id = post.question.id

    #users cannot vote on their own posts
    if request.user == post.user:
        r = ''

    u = UserProfile.user_manager.get(user = post.user)

    #check if the user has already voted on the post and up/down vote accordingly
    if upv:
        if upv[0].vote == 'up_vote':

            if vote_type == 'up_vote':
                post.up_votes -= 1
                u.rep_points-=5
                note = '-5  vote retracted [link to post which was un-voted]'
            else:
                post.up_votes -= 1
                post.down_votes += 1
                u.rep_points-=10
                note = '-10  upvote changed to downvote [link to post ]'
                upv_new.save()
        else:

            if vote_type == 'down_vote':
                post.down_votes -= 1
                u.rep_points+=5
                note = '+5  upvote [link to post ]'
            else:
                post.down_votes -= 1
                post.up_votes += 1
                u.rep_points +=10
                note = '+10  changed from downvote to upvote [link to post ]'
                upv_new.save()

        #do we need to save after delete?
        upv[0].delete()

    else:
        if vote_type == 'up_vote':
            post.up_votes+=1
            u.rep_points+=5
            note = '+5  your post recieved an upvote [link to post ]'
        else:
            post.down_votes+=1
            u.rep_points-=5
            note = '-5  your post recieved a downvote [link to post ]'

        upv_new.save()

    n = Notification(user=post.user, notification_body=note, created_date=timezone.now())

    n.save()
    post.save()
    u.save()

    #check if poster gets an achievement
    score = post.up_votes - post.down_votes
    if score%10 == 0 and score >= 10 and score <= 100:
        ach_name = str(score)+' score'
        a = Achievement.achievement_manager.get(achievement_name = ach_name)
        ua = User_Achievement.user_achievement_manager.filter(user = post.user, achievement = a)
        if not ua:
            new_ua = User_Achievement(user = post.user, achievement = a)
            new_ua.save()

            u.rep_points+=a.achievement_points
            u.save()

            note = 'congratulations!!! You have earned the achievement: '+a.achievement_name+'and have been awarded'+str(a.achievement_points)+'points.'
            n = Notification(
                user = post.user,
                notification_body = note,
                created_date = timezone.now()
            )
            n.save()

    url = '/question/'+str(question_id)

    #return redirect('question_thread',args=[post_id])
    return redirect(url)

def question_upvote(request,post_id):

    question = Question.question_manager.get(id=post_id)
    question.up_votes += 1
    question.save()

    qs = question.user
    #get profile of person who posted the question
    profile = UserProfile.user_manager.get(user = qs)
    profile.rep_points+=1
    profile.save()
    #get users which follow the user performing the upvote action
    ufus = User_Follows_User.user_follows_user_manager.filter(followed  = request.user)
    #notify them of this action
    note = 'someone you follow has liked somthing'
    for ufu in ufus:
        follower = ufu.follower
        n = Notification(user= follower, notification_body=note)
        n.save()

    url = '/question/' + str(post_id)
    return redirect(url)


def question_downvote(request, post_id):
    question = Question.question_manager.get(id=post_id)
    #uestion.user.rep_points+=1
   # profile = UserProfile.user_manager.get(question.user.user)
   # profile.rep_points+=1
    question.down_votes += 1
    question.user.save()
    question.save()
    url = '/question/' + str(post_id)
    return redirect(url)



def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')

def login_view(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect(reverse('index'))
    if request.method == 'GET':
        form = AuthenticationForm()
        return render(request, 'Login.html', {'form': form})
    if request.method == 'POST':
        form = AuthenticationForm(request=request, data=request.POST)
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None:
                print(user)
                if not request.POST.get('remember_me', None):
                    request.session.set_expiry(0)
                login(request, user)
                return HttpResponseRedirect('/index/')
            else:
                print('User not found')
        else:
            # If there were errors, we render the form with these
            # errors
            return render(request, 'Login.html', {'form': form})


def signup(request):
    if request.user.is_authenticated:
        return HttpResponseRedirect('/index/')
    if request.method == 'GET':
        form = UserregRegistrationForm()
        # form = UserCreationForm()
        return render(request, 'signup.html', {'form': form})
    if request.method == 'POST':
        form = UserregRegistrationForm(request.POST)
        # form = UserCreationForm(request.POST)
        if form.is_valid():
            # https://docs.djangoproject.com/en/1.11/topics/forms/modelforms/#the-save-method
            form.save()
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=password)
            login(request, user)
            return HttpResponseRedirect('/index/')
        else:
            # If there were errors, we render the form with these
            # errors
            return render(request, 'signup.html', {'form': form})


def index(request):

    # Data to send to the template
    context = {
        'questions': []
    }

    # Query the database
    questions = Question.question_manager.order_by('created_date')

    for question in questions:
        profile = UserProfile.user_manager.get(user = question.user)
        question_data = {
                'id': question.id,
                'title': question.question_title,
                'body': question.question_body,
                'date_posted': question.created_date,
                'date_modfied': question.modified_date,
                'upVotes': question.up_votes,
                'downVotes': question.down_votes,
                'num_answers': question.num_answers,
                'num_views': question.question_num_views,
                'topic_name': question.topic.topic_name,
                'user_name': question.user.username,
                'user_picture': profile.profile_picture,
                'reputation': profile.rep_points,
        }

        context['questions'].append(question_data)

    return render(request, 'index.html', context)



def help(request):

    # Data to send to the template
    context = {
        'help_items': []
    }

    # Query the database
    help_items = Help_Item.help_item_manager.all()

    for h in help_items:
        help_item_data = {
                'id': h.id,
                'body': h.help_body,
                'description': h.help_description,
        }

        context['help_items'].append(help_item_data)


    return render(request, 'help.html', context)

def help_item(request, help_id):

    # Query the database
    help_item = Help_Item.help_item_manager.get(id=help_id)

    # Data to send to the template
    context = {
        'help_item': {
            'id': help_item.id,
            'body': help_item.help_body,
            'description': help_item.help_description,
        }
    }

    return render(request, 'help_item.html', context)


def questions(request,page_number='1',filter='newest',unanswered = 'false'):

    # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)

    # data to send to the template
    context = {
        'questions': [],
        'next_page': next_page,
        'prev_page': prev_page
    }

    # Query the database
    # each users page should display 30 users, go to next users page to display next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    if filter == 'newest':
        order = 'created_date'
    else:
        order = 'up_votes'

    if unanswered == 'true':
        questions = Question.question_manager.filter(id__range=(lb, ub)).order_by(order)

    questions = Question.question_manager.filter(id__range=(lb, ub)).order_by(order)

    for question in questions:
        profile = UserProfile.user_manager.get(user = question.user)
        question_data = {
                'id': question.id,
                'title': question.question_title,
                'body': question.question_body,
                'date_posted': question.created_date,
                'date_last_modified': question.modified_date,
                'upVotes': question.up_votes,
                'downVotes': question.down_votes,
                'num_answers': question.num_answers,
                'num_views': question.question_num_views,
                'topic_name': question.topic.topic_name,
                'user_name': question.user.username,
                'user_picture': profile.profile_picture,
                'reputation': profile.rep_points,
        }

        context['questions'].append(question_data)


    return render(request, 'questions.html', context)


def question_thread(request, question_id, filter = 'votes'):

    # Query the database
    question = Question.question_manager.get(id = question_id)
    poster = UserProfile.user_manager.get(user = question.user)
    request_user = request.user

    q_is_owner = question.user == request_user

    form = AnswerForm()
    answerCommentForm = AnswerCommentForm()
    questionCommentForm = QustionCommentForm()


    try:
        check_if_favorited = User_Favorite_Questions.user_favorite_qeustions_manager.get(user = request.user, question = question)
    except User_Favorite_Questions.DoesNotExist:
        check_if_favorited = None

    if check_if_favorited==None:
        follows = False
    else:
        follows = True

    # Data to send to the template
    context = {
        'question':{
            'id':question.id,
            'title':question.question_title,
            'body' :question.question_body,
            'date_posted':question.created_date,
            'date_last_modified': question.modified_date,
            'upVotes':question.up_votes,
            'downVotes': question.down_votes,
            'num_answers':question.num_answers,
            'num_views':question.question_num_views,
            'num_comments':question.question_num_comments,
            'topic':question.topic.topic_name,
            'user_name':poster.user.username,
            'user_picture': poster.profile_picture,
            'user_rep': poster.rep_points,
            'form': form,
            'answers': [],
            'answerCommentForm':answerCommentForm,
            'questionCommentForm':questionCommentForm,
            'question_comments': [],
            'q_is_owner': q_is_owner,
            'follows': follows,
        }
    }

    for qc in question.question_comment_set.all():
        qc_is_owner = qc.user == request_user
        context['question']['question_comments'].append(
            {
                'id': qc.id,
                'body':qc.question_comment_body,
                'date_posted':qc.created_date,
                'date_modified': qc.modified_date,
                'upVotes':qc.up_votes,
                'downVotes':qc.down_votes,
                'user_name':qc.user.username,
                'qc_is_owner': qc_is_owner,
            }
        )

    #filter to order the questions
    if filter == 'newest':
        order = 'created_date'
    elif filter == 'oldest':
        order = '-created_date'
    else:
        order = 'up_votes'

    answers = question.answer_set.all().order_by(order)

    index = 0
    for a in answers:
        a_is_owner = a.user == request_user
        u = UserProfile.user_manager.get(user = a.user)
        context['question']['answers'].append({
            'id': a.id,
            'body': a.answer_body,
            'upVotes': a.up_votes,
            'downVotes': a.down_votes,
            'accepted':a.accepted,
            'date_posted': a.created_date,
            'date_modified': a.modified_date,
            'user_name': u.user.username,
            'user_picture': u.profile_picture,
            'user_rep': u.rep_points,
            'a_is_owner': a_is_owner,
            'answer_comments': [],
        })


        for ac in a.answer_comment_set.all():
            ac_is_owner = ac.user == request_user
            context['question']['answers'][index]['answer_comments'].append(
                {
                    'id': ac.id,
                    'body': ac.answer_comment_body,
                    'date_posted': ac.created_date,
                    'date_modified': ac.modified_date,
                    'upVotes': ac.up_votes,
                    'downVotes': ac.down_votes,
                    'user_name': ac.user.username,
                    'ac_is_owner':ac_is_owner,
                }
            )

        index+=1

    return render(request, 'question.html', context)

def topic(request, topic_id, page_number =1, filter ='newest', unanswered = 'false'):

    # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)

    # Query the database
    # each users page should display 30 users, go to next users page to display next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    if filter == 'newest':
        order = 'created_date'
    else:
        order = 'up_votes'

    # Query the database
    t = Topic.topic_manager.get(id=topic_id)

    if unanswered == 'true':
        topic_questions = Question.question_manager.filter(has_accepted_answer=False).filter(topic=t).order_by(order)[lb:ub]
    else:
        topic_questions = Question.question_manager.filter(topic=t).order_by(order)[lb:ub]

    try:
        check_if_follows = User_Topic.user_topic_manager.get(user=request.user, topic=t)
    except User_Topic.DoesNotExist:
        check_if_follows = None

    if check_if_follows==None:
        follows = False
    else:
        follows = True


    # Data to send to the template
    context = {
        'topic': {
            'id': t.id,
            'name': t.topic_name,
            'description': t.topic_description,
            'num_questions': t.topic_num_questions,
            'questions': [],
            'follows': follows,
        },
        'next_page': next_page,
        'prev_page': prev_page
    }

    for q in topic_questions:
        user_profile = UserProfile.user_manager.get(user = q.user)
        context['topic']['questions'].append({
            'id':q.id,
            'title':q.question_title,
            'body' :q.question_body,
            'date_posted':q.created_date,
            'date_last_modify':q.modified_date,
            'upVotes':q.up_votes,
            'downVotes': q.down_votes,
            'num_answers':q.num_answers,
            'num_views':q.question_num_views,
            'num_comments':q.question_num_comments,
            'topic':q.topic.topic_name,
            'user_picture':user_profile.profile_picture,
            'user_name':user_profile.user.username,
            'user_rep': user_profile.rep_points,
        })

    return render(request, 'topic.html', context)


def topics(request, page_number = '1', filter='topic_num_questions'):

    # calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int + 1)
    prev_page = str(page_number_int - 1)

    # data to send to the template
    context = {
        'topics': [],
        'next_page': next_page,
        'prev_page': prev_page
    }

    # Query the database
    # each users page should display 30 users, go to next users page to display next 30 users
    # what order should we choose?
    lb = 30 * (page_number_int - 1)
    ub = 30 * page_number_int

    if filter == 'name':
        order = 'topic_name'
    else:
        order = 'topic_num_questions'

    # Query the database
    topics = Topic.topic_manager.order_by(order)[lb:ub]

    for topic in topics:

        topic_data = {
                'id': topic.id,
                'name': topic.topic_name,
                'description': topic.topic_description,
                'num_questions': topic.topic_num_questions,
        }

        context['topics'].append(topic_data)

    return render(request, 'topics.html', context)


def achievements(request):

    context = {
        'achievements': [],
    }
    # Query the database
    achievements = Achievement.achievement_manager.order_by('achievement_points')

    for a in achievements:
        achievement_data = {
            'id': a.id,
            'name': a.achievement_name,
            'description': a.achievement_description,
            'icon': a.achievement_icon,
            'points':a.achievement_points,
        }

        context['achievements'].append(achievement_data)

    return render(request, 'achievements.html', context)


def user(request, user_id):

    
    # Query the database
    uu = User.objects.get(id = user_id)
    u = UserProfile.user_manager.get(user = uu)
    my_account = request.user == uu

    if not my_account:
        u.profile_views+=1

    # Data to send to the template
    context = {
        'user': {
            'id': u.id,
            'username': u.user.username,
            'description': u.user_description,
            'email': u.user.email,
            'picture': u.profile_picture,
            'num_followers': u.num_followers,
            'num_questions': u.num_questions_asked,
            'num_answers': u.num_answers_given,
            'member_since': u.created_date,
            'views': u.profile_views,
            'reputation': u.rep_points,
            'password': u.user.password,
            'activity_log':[],
        },
        'my_account': my_account
    }

    '''log = User_Activity_Log.user_activity_log_manager.filter(user = u.user).order_by('-created_date')

   
    for a in log:
        context['user']['activity_log'].append({
            'user': ac.user,
            'post_type': a.post_type,
            'post_description': a.post_description,
            'post_id': ac.id,
            'created_date': timezone.now(),
        })'''


    return render(request, 'user.html', context)

def users(request, page_number = '1', filter='newest'):

    #calculate index for next and previous page
    page_number_int = int(page_number)
    next_page = str(page_number_int+1)
    prev_page = str(page_number_int-1)

    # data to send to the template
    context = {
        'users':[],
        'next_page': next_page,
        'prev_page': prev_page
    }

    # Query the database
    #each users page should display 30 users, go to the next users page to display the next 30 users
    #what order should we choose?
    lb = 30*(page_number_int-1)
    ub = 30*page_number_int

    if filter == 'newest':
        order = 'created_date'
    else:
        order = 'rep_points'


    users = UserProfile.user_manager.order_by(order)[lb:ub]

    for user in users:

        try:
            check_if_follows = User_Follows_User.user_follows_user_manager.get(follower=request.user, followed=user.user)
        except User_Follows_User.DoesNotExist:
            check_if_follows = None

        if check_if_follows == None:
            is_following = False
        else:
            is_following = True

        my_account = request.user == user.user


        user_data = {
            'id': user.id,
            'user_name': user.user.username,
            'description': user.user_description,
            'picture': user.profile_picture,
            'num_followers': user.num_followers,
            'num_questions': user.num_questions_asked,
            'num_answers': user.num_answers_given,
            'member_since': user.created_date,
            'views': user.profile_views,
            'reputation': user.rep_points,
            'following': is_following,
            'my_account': my_account,
        }

        context['users'].append(user_data)

    return render(request, 'users.html', context)


def custom_404(request):
    return render(request, 'custom_404.html')


def search_questions(request):

    print(request.POST.get('s', ''))

    if request.method == 'POST':
        questions = SearchQuerySet().using('default').autocomplete(content_auto=request.POST.get('s', ''))
        print(questions[0].object.question_title)
        return render(request, 'results.html', {'questions': questions})


def search_topics(request):

    # # calculate index for next and previous page
    # page_number_int = int(page_number)
    # next_page = str(page_number_int + 1)
    # prev_page = str(page_number_int - 1)
    #
    # # data to send to the template
    # context = {
    #     'topics': [],
    #     'next_page': next_page,
    #     'prev_page': prev_page
    # }
    #
    # # Query the database
    # # each users page should display 30 users, go to next users page to display next 30 users
    # # what order should we choose?
    # lb = 5 * (page_number_int - 1)
    # ub = 5 * page_number_int

    if request.method == 'POST':
        topics = SearchQuerySet().using('autocomplete').autocomplete(content_auto=request.POST.get('s', ''))


        # for topic in topics:
        #     topic_data = {
        #         'id': topic.object.id,
        #         'name': topic.object.topic_name,
        #         'description': topic.object.topic_description,
        #         'num_questions': topic.object.topic_num_questions,
        #     }
        #
        #     context['topics'].append(topic_data)

        return render(request, 'topics_results.html', {'topics': topics})


def search_users(request):

    if request.method == 'POST':
        # print(request.POST.get('s', ''))
        users = SearchQuerySet().using('user').autocomplete(content_auto=request.POST.get('s', ''))
        return render(request, 'users_results.html', {'users': users})


def contact(request):
        return render(request, 'contact.html')


def error(request):
    return render(request, 'custom_404.html')






class UserProfileViewSet(viewsets.ModelViewSet):
        serializer_class = UserProfileSerializer
        queryset = User.objects.all()


class QuestionViewSet(viewsets.ModelViewSet):
        serializer_class = QuestionSerializer
        queryset = Question.objects.all()

class TopicViewSet(viewsets.ModelViewSet):
            serializer_class = TopicSerializaer
            queryset = Topic.objects.all()


