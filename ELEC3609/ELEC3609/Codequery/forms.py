from django import forms
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Question,Answer, UserProfile, Question_Comment, Answer_Comment, Message


class UserRegoForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ['username', 'email', 'password']


class UserregRegistrationForm(UserCreationForm):
    email = forms.EmailField(required=True)

    class Meta:
        model = User
        fields =[
            'username',
            'email',
            'first_name',
            'last_name',
            'password1',
            'password2'
        ]

    def save(self, commit=True):
        user = super(UserregRegistrationForm, self).save(commit=False)
        user.email = self.cleaned_data['email']
        user.first_name = self.cleaned_data['first_name']
        user.last_name = self.cleaned_data['last_name']

        if commit:
            user.save()

        return user

class QuestionForm(forms.ModelForm):
    class Meta:
        model = Question
        fields = ['question_title', 'question_body','topic']


class AnswerForm(forms.ModelForm):
    class Meta:
        model = Answer
        fields = ['answer_body']


class EditProfileForm(UserChangeForm):
    class Meta:
        model = User
        fields = [
            #'profile_picture',
            #'user_description',

            'password',
            'username',
            'email'
        ]

class QustionCommentForm(forms.ModelForm):
    class Meta:
        model = Question_Comment
        fields = ['question_comment_body']


class AnswerCommentForm(forms.ModelForm):
    class Meta:
        model = Answer_Comment
        fields = ['answer_comment_body']


class WriteMessageForm(forms.ModelForm):
    class Meta:
        model = Message
        fields = ['message_body']


class SearchForm(forms.Form):
    post = forms.CharField(label='', widget=forms.TextInput(attrs={'placeholder': 'Search'}))


# class Search(SearchForm):
#     start_date = forms.DateField(required=False)
#
#     def search(self):
#         sqs = super(SearchForm, self).search()
#
#         if not self.is_valid():
#             return self.no_query_found()
#
#         return sqs
