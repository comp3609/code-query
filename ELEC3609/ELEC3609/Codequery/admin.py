from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(UserProfile)
admin.site.register(Answer)
admin.site.register(Question)
admin.site.register(Topic)
admin.site.register(User_Favorite_Questions)
admin.site.register(Question_Comment)
admin.site.register(Answer_Comment)
admin.site.register(Message)
admin.site.register(Archived_Messages)
admin.site.register(Deleted_Messages)
admin.site.register(User_Topic)
admin.site.register(Notification)
admin.site.register(User_Follows_User)
admin.site.register(User_Question_Comment_Vote)
admin.site.register(User_Answer_Comment_Vote)
admin.site.register(User_Answer_Vote)
admin.site.register(User_Question_Vote)
admin.site.register(Achievement)
admin.site.register(User_Achievement)



admin.site.register(Help_Item)

admin.site.register(User_Activity_Log)

