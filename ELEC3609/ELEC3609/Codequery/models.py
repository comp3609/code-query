from django.db import models
import django.utils.timezone as timezone
import hashlib
import django
from django.core.files.storage import FileSystemStorage
from django.contrib.auth.models import User
from django.db.models.signals import post_save
# Create your models here.


class BaseModel(models.Model):
    created_date = models.DateTimeField(default=django.utils.timezone.now, editable=False)
    modified_date = models.DateTimeField(default=django.utils.timezone.now, editable=False)

    class Meta:
        abstract = True

class Votes(models.Model):
    up_votes = models.IntegerField(default=0)
    down_votes = models.IntegerField(default=0)

    class Meta:
        abstract = True

class Test(models.Model):
    name = models.CharField(max_length=20)

#profile for a user
class UserProfile(BaseModel):

    user = models.OneToOneField(User)
    profile_picture = models.ImageField(default='default_profile_pic.jpg',blank=True)
    user_description = models.TextField('Description',max_length=1000, blank=True, null=True)
    num_questions_asked = models.IntegerField(default=0)
    num_answers_given = models.IntegerField(default=0)
    rep_points = models.IntegerField(default=0)
    profile_views = models.IntegerField(default=0)
    num_followers = models.IntegerField(default=0)


    # Set Manager #
    objects = models.Manager()
    user_manager = models.Manager()

    def __str__(self):
        return self.user.email

#note need to take out null for forienkeys, but it causes issues atm
class Message(BaseModel):
    message_body = models.TextField('Message', max_length=1000)
    sender = models.ForeignKey(User, on_delete=models.CASCADE, related_name='sender', null = True)
    recipient = models.ForeignKey(User, on_delete=models.CASCADE, related_name='recipient', null = True)
    deleted = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)

    # Set Manager #
    message_manager = models.Manager()

    def __str__(self):
        return self.message_body

class Archived_Messages(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.ForeignKey(Message, on_delete=models.CASCADE)

    # Set Manager #
    archived_message_manager = models.Manager()

class Deleted_Messages(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message = models.ForeignKey(Message, on_delete=models.CASCADE)

    # Set Manager #
    deleted_message_manager = models.Manager()


class Achievement(BaseModel):
    achievement_description = models.TextField('Message', max_length=100)
    achievement_name = models.CharField(max_length=50 , unique=True)
    achievement_icon = models.CharField(max_length=200)
    achievement_points = models.IntegerField(default=0)

    # Set Manager #
    achievement_manager = models.Manager()

    def __str__(self):
        return self.achievement_name


class Topic(BaseModel):
    topic_description = models.TextField('Message', max_length=1000)
    topic_name = models.CharField(max_length=100)
    topic_num_questions=models.IntegerField(default=0)

    # Set Manager #
    objects = models.Manager()
    topic_manager = models.Manager()

    def __str__(self):
        return self.topic_name





class User_Achievement(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    achievement = models.ForeignKey(Achievement, on_delete=models.CASCADE)

    # Set Manager #
    user_achievement_manager = models.Manager()

    def __str__(self):
        return 'user_achievement'


class User_Topic(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

    # Set Manager #
    user_topic_manager = models.Manager()

    def __str__(self):
        return "user_topic"

class Notification(BaseModel):
    user = models.ForeignKey(User,on_delete = models.CASCADE, null=True)
    notification_body = models.TextField('Notification', max_length=1000)

    # Set Manager #
    notification_manager = models.Manager()

class User_Notification(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE)

    # Set Manager #
    user_notification_manager = models.Manager()

    def __str__(self):
        return ''


class Help_Item(BaseModel):
    help_body = models.TextField('Help', max_length=100)
    help_description= models.TextField('Help', max_length=1000)

    # Set Manager #
    help_item_manager = models.Manager()

    def __str__(self):
        return self.help_description


'''class User_Helpitem(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    helpitem = models.ForeignKey(Help_Item, on_delete=models.CASCADE)
    help_time = models.DateTimeField('Help Time',default=timezone.now)'''


class Question(BaseModel,Votes):
    question_title = models.CharField(max_length=100)
    question_body = models.TextField('Question', max_length=1000)
   # question_upVotes = models.IntegerField(default=0)
   # question_downVotes = models.IntegerField(default=0)
    num_answers = models.IntegerField(default=0)
    question_num_views = models.IntegerField(default=0)
    question_num_comments = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)
    has_accepted_answer = models.BooleanField(default=False)

    # Set Manager #
    objects = models.Manager()
    question_manager = models.Manager()


    def __str__(self):
        return self.question_title

class Answer(BaseModel, Votes):
   # answer_upVotes = models.IntegerField(default=0)
   # answer_downVotes = models.IntegerField(default=0)
    accepted = models.IntegerField(default=0)
    answer_body = models.TextField('Answer',max_length=1000)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, null=True)
    num_comments = models.IntegerField(default=0)

    # Set Manager #
    answer_manager = models.Manager()

    def __str__(self):
        return "answer"

class Answer_Comment(BaseModel,Votes):
    answer_comment_body = models.TextField('Answer comment', max_length=1000)
   # comment_upVotes = models.IntegerField(default=0)
   # comment_downVotes = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)

    # Set Manager #
    answer_comment_manager = models.Manager()

    def __str__(self):
        return self.answer_comment_body


class User_Favorite_Questions(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    # Set Manager #
    user_favorite_qeustions_manager = models.Manager()

    def __str__(self):
        return 'favrotie question'

class User_Follows_User(BaseModel):
    follower = models.ForeignKey(User, on_delete=models.CASCADE, related_name='follower')
    followed = models.ForeignKey(User, on_delete=models.CASCADE, related_name='followed')

    # Set Manager #
    user_follows_user_manager = models.Manager()

    def __str__(self):
        return 'user_follows_user'


class Question_Comment(BaseModel, Votes):
    question_comment_body = models.TextField('Question comment', max_length=1000)
  #  comment_upVotes = models.IntegerField(default=0)
  #  comment_downVotes = models.IntegerField(default=0)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    question = models.ForeignKey(Question, on_delete=models.CASCADE)

    # Set Manager #
    question_comment_manager = models.Manager()

    def __str__(self):
        return self.question_comment_body



#this model stores users acitivty. log entries consist of a description of what the user did, such as for exaple, if they
#left an answer on a question or left a comment, or posted a question and so on.
class User_Activity_Log(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    post_type = models.CharField(max_length=100)
    post_description = models.CharField(max_length=100)
    post_id = models.IntegerField(default=0)

    user_activity_log_manager = models.Manager()

    def __str__(self):
        return self.post_description

class User_Question_Vote(BaseModel):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.CharField(max_length=20)

    user_question_vote_manager = models.Manager()


class User_Question_Comment_Vote(BaseModel):
    qc = models.ForeignKey(Question_Comment, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.CharField(max_length=20)

    user_question_comment_vote_manager = models.Manager()


class User_Answer_Comment_Vote(BaseModel):
    ac = models.ForeignKey(Answer_Comment, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.CharField(max_length=20)

    user_answer_comment_vote_manager = models.Manager()


class User_Answer_Vote(BaseModel):
    answer = models.ForeignKey(Answer, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    vote = models.CharField(max_length=20)

    user_answer_vote_manager = models.Manager()

