from rest_framework import serializers
from .models import UserProfile, Question, Topic
from rest_framework.request import Request



class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = ('url','user','user_description','num_questions_asked','num_answers_given','rep_points','profile_views')


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('url','question_title','question_body','num_answers','user','topic')

class TopicSerializaer(serializers.ModelSerializer):
    class Meta:
        model = Topic
        fields = ('url','topic_description','topic_name','topic_num_questions')


