from django.conf.urls import url, include
from django.contrib import admin
#from . import view
#from Codequery import Login, Register



urlpatterns = [
    url(r'^', include('Codequery.urls')),
    url(r'^admin/', admin.site.urls),
    #url(r'^messages/', include('postman.urls', namespace='postman', app_name='postman')),
]

handler404 = 'Codequery.views.custom_404'