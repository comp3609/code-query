import MySQLdb
import os
import sys
import django
import datetime
from django.utils import timezone

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ELEC3609.settings')
django.setup()

db = MySQLdb.connect(
    host="127.0.0.1",
    user="root",
    passwd="451802",
    db="code_query",
    use_unicode=True
)
cur = db.cursor()
db.autocommit(on=True)

now = timezone.now()

# ----------------------------
# Empty Database
# ----------------------------
if len(sys.argv) == 2:
    print("\nSetting up empty database.\n")
    query = 'DROP DATABASE IF EXISTS code_query'
    cur.execute(query)
    query = 'CREATE DATABASE code_query'
    cur.execute(query)
    os.system(sys.argv[1] + " manage.py makemigrations Codequery")
    os.system(sys.argv[1] + " manage.py migrate")
    print("\nCreating default user account\n")
    os.system(sys.argv[1] + " manage.py createsuperuser")
else:
    print("Please run python setup-database.py [py/python/python3]")
    print("py/python/python3 depends on what system you are using.")
    exit(0)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ELEC3609.settings')
django.setup()

db = MySQLdb.connect(
    host="127.0.0.1",
    user="root",
    passwd="451802",
    db="code_query",
    use_unicode=True
)

cur = db.cursor()
db.autocommit(on=True)

now = timezone.now()

# -----------------------------------------
# Import projects from Sonya's project list
# -----------------------------------------

f = open("default_data/modeltopics-data.csv")
header = f.readline()
try:
    for line in f.readlines():
        topic = line.split('|')
        data = [None for x in range(3)]
        for i in range(len(topic)):
            if topic[i]:
                attr = topic[i].strip()
                data[i] = attr.strip("\"")

        query = "INSERT INTO codequery_topic(created_date, modified_date, topic_description, topic_name, top_num_questions) VALUES (%s, %s, %s, %s, %s)"
        cur.execute(query, (now, now, data[0], data[1], data[2]))

    print("Successfully imported all topics!")
except Exception as e:
    print(e)
    print("WARNING: There was an error importing the topics.\n")
f.close()
# ---------
# Finished!
# ---------

db.close()
print("\nLooks like I'm finished! Check your codequery database.\n")
