# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ##

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## How do I get set up? ##

To start, you want to get a local copy of the project folder, stored in bitbucket. 
Bitbucket hosts the project repository online and allows team members to make a local copy of the repository on their machine.
In your local copy you can modify what you like without changing the online version. If you wish to apply the changes you have made 
locally to the online repository, you will have to use Sourcetree (more on this later). Perform the following steps to make a local copy of the repository.

### Get the code and version control

* Download SourceTree: [https://www.sourcetreeapp.com/](https://www.sourcetreeapp.com/)
* Go to our BitBucket repo: https://bitbucket.org/comp3609/code-query/overview
* Clone the repository. This may need to be done differently depending on the os you have.
  Sourcetree can be very buggy.
  
For mac try the following:
  
* Click the download icon on the overview page (right underneath the title)
* Click "Clone in SourceTree"
  
This may also work for windows (depending on version), but if it does not, try this:

* Copy the link next to the download button (make sure you take out the git command before the link)
* Go to sourcetree
* Click the clone link in the navigation bar
* Paste the repository link in the source path box (this should automatically create
  a destination folder, you can change this if you want. This will be where your local
  copy of the repository is stored)

* Leave the local folder box as [Root]
* Click clone

If you completed the above steps, this is what you have accomplished: you have made a local
copy of the repository stored online. You can modify your local copy without changing anything
in the online repository (unless you commit and push these change in sourcetree). 

### Edit the code

* Download PyCharm IDE: https://www.jetbrains.com/pycharm/

### Run the code


#### Download and install Python

* Download and install the latest version of python, make sure you check "add to path" when installing. 

#### Download and install Django

* Details: https://docs.djangoproject.com/en/1.11/topics/install/#installing-official-release

### Install MySQL community edition

* MySQL: https://dev.mysql.com/downloads/mysql/
* Video for installing MySQL on windows: [https://www.youtube.com/watch?v=WuBcTJnIuzo](https://www.youtube.com/watch?v=WuBcTJnIuzo)

In addition, you might want to look at SequelPro, If you are using mac.

* SequelPro: https://www.sequelpro.com/

After you have installed MySQL, do the following:

* Create a database user called code_query with password pgroup2
* Create an empty database called code_query

### Install Python MySQL wrapper

* Details: https://docs.djangoproject.com/en/1.11/ref/databases/#mysql-notes
* Summary: - Install MySQL client https://pypi.python.org/pypi/mysqlclient

### Run Django!

* navigate to your local repository in command line (folder where manage.py is)
* type: python manage.py runserver

### Things that still need to be sorted

* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

## Contribution guidelines ##

* Writing tests
* Code review
* Other guidelines

## Who do I talk to? ##

* Repo owner or admin
* Other community or team contact

## Development Guidelines ##

### This is the way we will be using bitbucket/sourcetree:

#### issues

In the bitbucket navbar, you should see issues listed. We will use this feature to keep track of what team members are currently working on (link to issues documentation: https://confluence.atlassian.com/bitbucket/use-the-issue-tracker-221449750.html). 

Before you start working on a given task, create an issue. An is issue is just a description of a task you will be working on. When you create an issue you can assign it to someone, if you intend on working on that particular issue then you would assign it to yourself . Issue have a couple of other fields which you must specify, you can see these when creating an issue. Here is a example of using issues, which is taken form a public bitbucket repository: https://bitbucket.org/site/master/issues. An issue has states, please use the following convention for state transitions:

new -> open (working on it) -> resolved (ready for testing) -> closed (passed tests) 

#### branching

After you have created an issue and assigned it to yourself create a new branch related to that issue. Braches are basically a alternated workflow. The main branch for any bitbucket repository is the master branch. When you create a new branch, you must branch of an other branch. What this does is you get a copy of everything that was in the branch you branched of, that is, you get a copy of the same project folder and files in the parent branch, but now if you make changes to these files they will not effect the parent branch, thus creating an alternate workflow. Below (at the end) you will find a link where you can learn more about branches and other git concepts.

The new branch you create should adopt the following name convention:

dev/[name]/issue-[number]-[task name]

for example: dev/charlie/issue-5-create-home-page.

All branches should branch from the develop branch, or from other issue related branches if you are working on a sub-issue. Never branch form the master. 

The master branch should be left untouched, as changing it can cause serious problems.

#### checkout

Once you have created your branch, go to SourceTree and open the CodeQuery repository. On the left panel click on remote then origin you should then see a list of all the branches for the CodeQuery repository. Checkout any branch by double clicking on it or right click the choose checkout. Now you should be able to see this branch listed under BRACHES. You can checkout any number of branches you want. Next double click on the branch you wish to work on, this will set the state of your local repository to that of the branch you clicked on. You should now see a circle next to the branch name, and the name should ne in bold. This means your local repository is set to the branch. 

Now you are ready to work on your issue. Open up the project in PyCharm and start working. Any changes you make will only be applied to the branch you are working on.

#### pushing

After you have made some changes witch you wish to upload online, so team member can see them. Go to SourceTree. Click on working copy under the  FILE STATUS menu item in the left panel. Under unstaged files you can see all the changes you have made. Clicking on one of the changes you can see the details of the change on the right. If you are happy with a change you can commit that change. To do this click on the change under unstaged files, and then click stage selected. In the box underneath. You can write a short description of the change you have made (you should always do this). After writing the descriptionm you then click commit. What is does is confirm the change you have made. After you commit, you should see a number next to the push button.
clicking the push button will allow you to upload these changes to the online version of your repository (in windows, not sure how it works on mac, you will be asked to confirm which branch
you want to push. Be very careful to read these sort of messages. They will come up for other things aswell. Aswell double checked that what you are doing is correct, as making changes to the worng files could cause
a lot oh problems).

#### merging

If you want your changes to be reflected in the main branch of development (the develop branch, in our case), then you must merge your branch
will develop. Be warned merging the most risky operation to perform. The details of merging will not be addressed here, as they are long and complicated; instead it is
advised that you research merging and in particular merge conflicts. A video of source merge conflects is provided at the end. The final result of merging is that
you added the changes you have made on your branch into the branch you merge to (the parent branch). Note is probebly a good idea
to notify the team before merging into the develop.

A safer form of merging, one which you will have to do frequently, is merging the devlop branch into your issue branch. This will add the latest changes in develop to your own branch.
You can merge branches by doing the following:

* make sure the branch you are working on is the current working copy in sourcetree
* right click on the branch you wish to merge into your local copy
* click merge into current branch

#### Pulling

If you see a number with a down arrow next to a branch, in sourcetree, that means there are changes in the online branch which
your local copy of that branch doesn't have. to get these changes into your local copy of that branch you will have to change rou working directory to that branch
(double click on the branch you want ot chnage to) and then click the pull button. Be warned that this is perform a merge operation so that your local code may conflict with
that of the online version.

#### do your research

The concepts used in bitbucket and sourcetree come from git. From wikipedia:

"Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people"

So to truely understand bitbucket and sourcetree you must know a little about git. The following is a detialed guide to git:

Detailed guide to git: https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud

You should find all you need to know about git in there.

While we will not be using git directly (sourcetree is a gui version of git), you can still apply everything you learn
in the tutorials to our cirsumstances. It is also advised that you research these topics yourself (google things when you don't understand). What has
been covered here is a breif summary of how we will be using bitbucket/sourcetree. It is by no means a full description
of these things, but should give you almost everything you need to work on the project.

video on sourcetree merge conflicts: https://www.youtube.com/watch?v=w-WbfmneIEI
